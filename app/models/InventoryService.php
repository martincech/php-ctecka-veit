<?php

namespace App\Models;
use PestJSON;

class InventoryService
{
	/**
	 * @var PestJSON
	 */
	private $restClient;

	const SET_AMOUNT_URL       = "?action=setAmount";
	const SET_AMOUNT_PLACE_URL = "?action=setAmountAndPlace";
	const INCREASE_AMOUNT_URL  = "?action=increaseAmount";
	const DECREASE_AMOUNT_URL  = "?action=transferToOperations";
	const GET_INFO_URL         = "?action=getInfo";
	const GET_PLACE_INFO_URL   = "?action=getPlaceInfo";
	const GET_PLACE_ITEMS_URL  = "?action=getPlaceItems";  
	const SET_PLACE_URL        = "?action=setPlace";
	const GET_ORDER_INFO_URL   = "?action=getOrderInfo";
	const GET_ORDER_ITEMS_URL  = "?action=getOrderItems";
	const GET_ORDER_TEXT_ITEMS_URL  = "?action=getOrderTextItems";
	const SET_ORDER_STATUS_URL  = "?action=setOrderStatus";  
	const SET_NEW_ORDER_REQUEST_URL  = "?action=newOrderRequest";
	const GET_COMPLETION_NORM_ID_URL   = "?action=getCompletionNormId";
	const GET_COMPLETION_NORM_ITEMS_URL  = "?action=getCompletionNormItems";  

	function __construct(PestJSON $restClient)
	{
		$this->restClient = $restClient;
	}

	function setNumberFormat($amount) {
		// API (ABRA) need FLOAT and DECIMALS with a COMMA
		return number_format($amount, 5, ',', '');
	}


	function setAmount ($barcode, $amount) {
		$fAmount = $this->setNumberFormat($amount);
		$this->restClient->post($this->restClient->base_url . self::SET_AMOUNT_URL . "&barcode=" . rawurlencode($barcode) . "&amount=" . rawurlencode($fAmount), []);
	}

	function setAmountAndPlace ($barcode, $placeid, $amount) {
		$fAmount = $this->setNumberFormat($amount);
		$this->restClient->post($this->restClient->base_url . self::SET_AMOUNT_PLACE_URL . "&barcode=" . rawurlencode($barcode) . "&placeid=" . rawurlencode($placeid) . "&amount=" . rawurlencode($fAmount), []);
	}

	function increaseAmount($barcode, $amount) {
		$fAmount = $this->setNumberFormat($amount);
		$this->restClient->post($this->restClient->base_url . self::INCREASE_AMOUNT_URL . "&barcode=" . rawurlencode($barcode) . "&amount=" . rawurlencode($fAmount), []);
	}

	function increaseAmountByOrder($barcode, $amount, $orderitemid) {
		$fAmount = $this->setNumberFormat($amount);
		$this->restClient->post($this->restClient->base_url . self::INCREASE_AMOUNT_URL . "&barcode=" . rawurlencode($barcode) . "&amount=" . rawurlencode($fAmount) . "&orderitemid=" . rawurlencode($orderitemid), []);
	}

	function increaseAmountByOrderSetPlace($barcode, $amount, $orderitemid, $placeid) {
		$this->increaseAmountByOrder($barcode, $amount, $orderitemid);
		if ($placeid) {
		$this->restClient->post($this->restClient->base_url . self::SET_PLACE_URL . "&barcode=" . rawurlencode($barcode) . "&placeid=" . rawurlencode($placeid), []);
		}
	}

	function decreaseAmount ($barcode, $amount) {
		$fAmount = $this->setNumberFormat($amount);
		$this->restClient->post($this->restClient->base_url . self::DECREASE_AMOUNT_URL . "&barcode=" . rawurlencode($barcode) . "&amount=" . rawurlencode($fAmount), []);
	}

	function getInfo ($barcode) {
		return $this->restClient->get($this->restClient->base_url . self::GET_INFO_URL . '&barcode=' . rawurlencode($barcode));
	}

	function getPlaceInfo ($placecode) {
		$encodedPlaceCode = rawurlencode($placecode);
		return $this->restClient->get($this->restClient->base_url . self::GET_PLACE_INFO_URL . '&placecode=' . $encodedPlaceCode);
	}

	function getPlaceItems ($placecode) {
		$encodedPlaceCode = rawurlencode($placecode);
		return $this->restClient->get($this->restClient->base_url . self::GET_PLACE_ITEMS_URL . '&placecode=' . $encodedPlaceCode);
	}
  
	function getCompletionNormId ($normcode) {
		$encodedNormCode = rawurlencode($normcode);
		return $this->restClient->get($this->restClient->base_url . self::GET_COMPLETION_NORM_ID_URL . '&normcode=' . $encodedNormCode);
	}

	function getCompletionNormItems ($normcode) {
		$encodedNormCode = rawurlencode($normcode);
		return $this->restClient->get($this->restClient->base_url . self::GET_COMPLETION_NORM_ITEMS_URL . '&normcode=' . $encodedNormCode);
	}

	function getOrderInfo ($prefix, $number, $year) {
		return $this->restClient->get($this->restClient->base_url . self::GET_ORDER_INFO_URL . '&prefix=' . rawurlencode($prefix) . '&number=' . rawurlencode($number) . '&year=' . rawurlencode($year));
	}

	function getOrderItems ($orderid) {
		return $this->restClient->get($this->restClient->base_url . self::GET_ORDER_ITEMS_URL . '&orderid=' . rawurlencode($orderid));
	}

	function getOrderTextItems ($orderid) {
		return $this->restClient->get($this->restClient->base_url . self::GET_ORDER_TEXT_ITEMS_URL . '&orderid=' . rawurlencode($orderid));
	}

	function setOrderStatus ($orderid, $status, $value) {
		return $this->restClient->post($this->restClient->base_url . self::SET_ORDER_STATUS_URL . '&orderid=' . rawurlencode($orderid) . '&status=' . rawurlencode($status) . '&value=' . rawurlencode($value), []);
	}

	function setNewOrderRequest ($barcode, $amount, $ownerLoginName) {
		$fAmount = $this->setNumberFormat($amount);
    $encodedOwnerName = rawurlencode($ownerLoginName);
		return $this->restClient->post($this->restClient->base_url . self::SET_NEW_ORDER_REQUEST_URL . '&barcode=' . rawurlencode($barcode) . '&amount=' . rawurlencode($fAmount) . '&owner=' . rawurlencode($encodedOwnerName), []);
	}
}
