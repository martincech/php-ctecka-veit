<?php

namespace App\Models;
use Pest;

class InventoryServiceDummy
{
	function __construct()
	{
	}

	function setAmount ($barcode, $amount) {
		//$this->restClient->post(self::SET_AMOUNT_URL, ["barcode"=>$barcode, "amount"=>$amount]);
	}

	function increaseAmount($barcode, $amount) {}

	function decreaseAmount ($barcode, $amount) {
		//$this->restClient->post(self::DECREASE_AMOUNT_URL, ["barcode"=>$barcode, "amount"=>$amount]);
	}

	function getInfo ($barcode) {
		//$this->restClient->get(self::GET_INFO_URL, ["barcode"=>$barcode]);
		return ["barcode"=>$barcode, 
			"name"=>"lorem",
			"quantity"=>11,
			"quantity_ordered"=>4,
			"quantity_demanded"=>0];
	}
}

