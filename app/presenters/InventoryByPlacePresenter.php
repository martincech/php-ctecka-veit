<?php

namespace App\Presenters;
use App\Components\InventoryForm;
use App\Components\SearchOrderForm;
use App\Components\OrderPrefixForm;
use App\Components\OrderYearForm;
use App\Components\YesNoForm;
use App\Components\PlaceForm;
use Nette\Application\UI;

class InventoryByPlacePresenter extends AbstractProductAmountPlaceModificationPresenter
{
	protected $pageName = "Inventura dílčí";
	protected $pageColor = "blue lighten-3";
	protected $pageIcon = "img/inventory.png";

	function renderItemAction($placecode, $item)
	{    
    $items = $this->inventoryService->getPlaceItems($placecode);
    $itemsCount = count($items);    
    if ($itemsCount == 0) {
      $items = $this->inventoryService->getCompletionNormItems($placecode);
      $itemsCount = count($items);    
    }            

		if (isset($item) && $item >= 0 && $item < $itemsCount) {    			
			$this->barcode = $items[$item]["code"];    
			$this->template->pageName = $this->pageName;
			$this->template->pageIcon = $this->pageIcon;
			$this->template->pageColor = $this->pageColor;
			$this->template->itemInfo = $this->inventoryService->getInfo($this->barcode);
		}
    else if ($itemsCount == 0) {
      $this->redirectToSearch("Žádné položky pro kód '" . $placecode . "'");
    }
		else {
      $this->redirectToSearch("Inventura kódu '" . $placecode . "' dokončena, celkem " . $itemsCount . " položky");
		}
	}

	function createComponentPlaceForm()
	{
		return new PlaceForm($this);
	}

	function onPlaceSubmitted(UI\Form $form, $values)
	{
    $this->redirect("itemAction", ["placecode"=>$values["placecode"], "item"=>0]);
	}

	function onInventorySubmitted(InventoryForm $form, $values)
	{
    $this->inventoryService->setAmount($values["barcode"], $values['amount']);
		$itemInfo = $this->inventoryService->getInfo($values["barcode"]);
    
    $placecode =  $this->request->parameters["placecode"];
		
    $storeQuantity = $itemInfo["quantity"];
		$amountFloatWithPoint = $this->amountFloatWithPoint($storeQuantity);
		if(isset($itemInfo) && $amountFloatWithPoint == $values['amount']) {
      $item = $this->request->parameters["item"] + 1;

      $backlinkParams = "placecode=>".$placecode.",item=>".$item;
      $this->checkStoreLimit($values["barcode"], "InventoryByPlace:item-action", $backlinkParams);
			
      $this->flashMessage("Nastaveno " . $values["barcode"] . ", sklad: " . $storeQuantity . "ks");          
  		$this->redirect("itemAction", ["placecode"=>$placecode, "item"=>$item]);      
		}
		else {
      $item = $this->request->parameters["item"];
			$this->flashMessage("Nastavení početu kusů se nezdařilo. Opakujte zadání znovu.");      
  		$this->redirect("itemAction", ["placecode"=>$placecode, "item"=>$item]);
		}
	}
}
