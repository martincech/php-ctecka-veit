<?php

namespace App\Presenters;
use App\Components\InventoryForm;

abstract class AbstractProductAmountModificationPresenter extends AbstractProductSearchPresenter
{
	protected $barcode;

	function createComponentInventoryForm()
	{
		if (!isset($this->barcode) && isset($this->request->parameters["barcode"])) {
			$this->barcode = $this->request->parameters["barcode"];
		}
		return new InventoryForm($this, $this->barcode);
	}

	abstract function onInventorySubmitted(InventoryForm $form, $values);
}
