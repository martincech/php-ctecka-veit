<?php

namespace App\Presenters;
use App\Components\InventoryForm;


class DeliverPresenter extends AbstractProductAmountModificationPresenter
{
	protected $pageName = "Vyskladnění";
	protected $pageColor = "red darken-1";
	protected $pageIcon = "img/storeout.png";

	function onInventorySubmitted(InventoryForm $form, $values)
	{
		$this->inventoryService->decreaseAmount($values["barcode"], $values['amount']);
		$this->checkStoreLimit($values["barcode"], "Deliver:search");

		$this->flashMessage("Vyskladněno");
		$this->redirect("search", ["barcode"=>$values["barcode"]]);
	}

}
