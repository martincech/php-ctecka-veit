<?php

namespace App\Presenters;
use App\Components\InventoryForm;

class StorePresenter extends AbstractProductAmountModificationPresenter
{
	protected $pageName = "Naskladnění položky";
	protected $pageColor = "green lighten-3";
	protected $pageIcon = "img/storein.png";

	function onInventorySubmitted(InventoryForm $form, $values)
	{
		$this->flashMessage("Naskladněno");
		$this->inventoryService->increaseAmount($values["barcode"], $values['amount']);
		$this->redirect("search", ["barcode"=>$values["barcode"]]);
	}
}
