<?php

namespace App\Presenters;
use App\Components\InventoryForm;
use App\Components\SearchOrderForm;
use App\Components\OrderPrefixForm;
use App\Components\OrderYearForm;
use App\Components\YesNoForm;
use App\Components\PlaceForm;
use Nette\Application\UI;

class StoreByOrderPresenter extends AbstractProductAmountPlaceModificationPresenter
{
	protected $pageName = "Naskladnění objednávky";
	protected $pageColor = "green lighten-1";
	protected $pageIcon = "img/storein-order.png";

	function renderGetPrefix($ordercode, $orderyear)
	{
		$this->template->pageName = $this->pageName;
		$this->template->pageColor = $this->pageColor;
		$this->template->pageIcon = $this->pageIcon;
		$this->template->ordercode = $ordercode;
		$this->template->year = $orderyear;
	}

	function renderGetYear($ordercode)
	{
		$this->template->pageName = $this->pageName;
		$this->template->pageColor = $this->pageColor;
		$this->template->pageIcon = $this->pageIcon;
		$this->template->ordercode = $ordercode;
	}

	function renderDescription($orderid, $items, $item, $description)
	{
		$this->template->pageName = $this->pageName;
		$this->template->pageIcon = $this->pageIcon;
		$this->template->pageColor = $this->pageColor;
		$this->template->description = $description;
	}

	function renderGetPlace($orderid, $items, $item, $barcode, $itemname)
	{
		$this->template->pageName = $this->pageName;
		$this->template->pageIcon = $this->pageIcon;
		$this->template->pageColor = $this->pageColor;
    
    $this->template->barcode = $barcode;
    $this->template->itemname = $itemname;    
	}

	function renderItemAction($orderid, $items, $item, $newplaceid="", $newplacelabel="")
	{    
		$orderItems = $this->inventoryService->getOrderItems($orderid);
		$orderTextItems = $this->inventoryService->getOrderTextItems($orderid);

		if ($item >= $items) {
			$this->redirect("textItemAction", ["orderid"=>$orderid, "openItems"=>$items]);
		}
		else if (isset($item) && $item >= 0) {			
			if ($orderItems[$item]["place"] == "--" && $newplaceid == "") {			
        $this->redirect("getPlace", ["orderid"=>$orderid, "items"=>$items, "item"=>$item, "barcode"=>$orderItems[$item]["code"], "itemname"=>$orderItems[$item]["name"]]);
			}
			else {
				$this->barcode = $orderItems[$item]["code"];
				$this->template->pageName = $this->pageName;
				$this->template->pageIcon = $this->pageIcon;
				$this->template->pageColor = $this->pageColor;
				$this->template->orderId = $orderid;
				$this->template->newplacelabel = $newplacelabel;
				$this->template->orderItemInfo = $orderItems[$item];
				$this->template->storeItemInfo = $this->inventoryService->getInfo($this->barcode);
			}
		}
		else {
			$this->redirectToSearch("Nezadaná položka objednávky");
		}
	}

	function renderTextItemAction($orderid, $openItems)
	{
		$orderTextItems = $this->inventoryService->getOrderTextItems($orderid);
		if ($openItems == 0) {
			if (count($orderTextItems) > 0) {
				$this->template->pageName = $this->pageName;
				$this->template->pageIcon = $this->pageIcon;
				$this->template->pageColor = $this->pageColor;
				$this->template->textOrders = $orderTextItems;
			}
			else {
				$this->CloseOrder($orderid, true);
				$this->redirectToSearch("Objednávka naskladněna");
			}
		}
		else {
			$this->redirectToSearch("Objednávka částečně naskladněna");
		}
	}


	function createComponentSearchOrderForm()
	{
		return new SearchOrderForm($this);
	}

  function createComponentOrderPrefixForm()
  {
    // return new OrderPrefixForm($this, $this->request->parameters["ordercode"], $this->request->parameters["orderyear"]);
    return new OrderPrefixForm($this, $this->request->parameters["ordercode"]);
  }

  function createComponentOrderYearForm()
  {
    return new OrderYearForm($this, $this->request->parameters["ordercode"]);
  }

  function createComponentYesNoForm()
  {
    return new YesNoForm($this);
  }

  function createComponentYesNoDescriptionForm()
  {
    return new YesNoForm($this, "onYesNoDescriptionSubmitted");
  }


	function onSearchOrderSubmitted(UI\Form $form, $values)
	{
		$orderCode = $values["ordercode"];
		$orderCodePattern = "/([OVov]{2}[1-2])-(\d+)\/(\d{4})/";
		preg_match($orderCodePattern, $orderCode, $splitCode);
		$isFullOrderFormat = !empty($splitCode[0]);

		if ($isFullOrderFormat) {
			$prefix = $splitCode[1];
			$number = $splitCode[2];
			$year = $splitCode[3];
			$this->checkOrderExistAndRedirect($prefix, $number, $year);
		} else {
			// $this->redirect("getYear", ["ordercode"=>$values["ordercode"]]);
			$this->redirect("getPrefix", ["ordercode"=>$values["ordercode"], "orderyear"=>'']);
		}
	}

	function onOrderYearSubmitted(UI\Form $form, $values)
	{
		$this->redirect("getPrefix", ["ordercode"=>$values["ordercode"], "orderyear"=>$values["orderyear"]]);
	}

	function onOrderPrefixSubmitted(UI\Form $form, $values)
	{
		$prefix = "OV" . $values["orderprefix"];
		$number = $values["ordercode"];
		$year = $values["orderyear"];
		$this->checkOrderExistAndRedirect($prefix, $number, $year);
	}

	function checkOrderExistAndRedirect($prefix, $number, $year)
	{
		$order = $this->inventoryService->getOrderInfo($prefix, $number, $year);
		if (empty($order)) {
			$this->redirectToSearch("Ojednávka nenalezena");
		} else {
			$orderItems = $this->inventoryService->getOrderItems($order['id']);
			if ($order["description"] <> "") {
				$this->redirect("description", ["orderid"=>$order['id'], "items"=>count($orderItems), "item"=>0, "description"=>$order["description"]]);
			}
			else {
				$this->redirect("itemAction", ["orderid"=>$order['id'], "items"=>count($orderItems), "item"=>0]);
			}
		}
	}

	function onPlaceSubmitted(UI\Form $form, $values)
	{
		$placeInfo = $this->inventoryService->getPlaceInfo($values["placecode"]);
		$barcode = $this->request->parameters["barcode"];
    $itemname = $this->request->parameters["itemname"];  
    $orderid = $this->request->parameters["orderid"];
		$items = $this->request->parameters["items"];
		$item = $this->request->parameters["item"];

		if (empty($placeInfo)) {
			$this->flashMessage("Umístění nenalezeno");
			$this->redirect("getPlace", ["orderid"=>$orderid, "items"=>$items, "item"=>$item, "barcode"=>$barcode, "itemname"=>$itemname]);
		} else {
			$this->redirect("itemAction", ["orderid"=>$orderid, "items"=>$items, "item"=>$item, "newplaceid"=>$placeInfo["id"], "newplacelabel"=>$placeInfo["code"]]);
		}
	}

	function onInventorySubmitted(InventoryForm $form, $values)
	{
		$this->inventoryService->increaseAmountByOrderSetPlace($values["barcode"], $values['amount'], $values['orderitemid'], $values['placeid']);

		$itemInfo = $this->inventoryService->getInfo($values["barcode"]);
		$storeQuantity = $itemInfo["quantity"];
		$this->flashMessage("Naskladněno " . $values["barcode"] . ", sklad: " . $storeQuantity . "ks");

		$orderid =  $this->request->parameters["orderid"];

		$orderItems = $this->inventoryService->getOrderItems($orderid);
		$items =  $this->request->parameters["items"];
		$item = $this->request->parameters["item"];
		if ($items > count($orderItems)) {
			$items = count($orderItems);
		}
		else {
			$item = $this->request->parameters["item"] + 1;
		}

		$this->redirect("itemAction", ["orderid"=>$orderid, "items"=>$items, "item"=>$item]);
	}

	function onYesNoSubmitted(UI\Form $form, $values)
	{
		$orderid =  $this->request->parameters["orderid"];
		if ($values["answer"]) {
			$this->CloseOrder($orderid, true);
			$this->redirectToSearch("Objednávka naskladněna a vyřízena");
		}
		else {
			$this->CloseOrder($orderid, false);
			$this->redirectToSearch("Naskladněno bez textových položek");
		}
	}

	function onYesNoDescriptionSubmitted(UI\Form $form, $values)
	{
		$orderid =  $this->request->parameters["orderid"];
		$items =  $this->request->parameters["items"];
		if ($values["answer"] == 1) {
			$this->redirect("itemAction", ["orderid"=>$orderid, "items"=>$items, "item"=>0]);
		}
		else {
			$this->redirectToSearch("Naskladnění přerušeno");
		}
	}

	function CloseOrder($orderid, $closed = true)
	{
		if ($closed) {
			$this->inventoryService->setOrderStatus($orderid, "Closed", true);
			$this->inventoryService->setOrderStatus($orderid, "Confirmed", true);
		}
		else {
			$this->inventoryService->setOrderStatus($orderid, "Closed", false);
		}
	}
}
