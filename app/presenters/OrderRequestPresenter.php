<?php

namespace App\Presenters;
use App\Components\InventoryForm;


class OrderRequestPresenter extends AbstractProductAmountModificationPresenter
{
	protected $pageName = "Požadavek skladu";
	protected $pageColor = "purple lighten-1";
	protected $pageIcon = "img/request.png";

	function onInventorySubmitted(InventoryForm $form, $values)
	{
		$ownerLoginName = "Reader";
		$this->flashMessage("Požadavek skladu vytvořen");
		$this->inventoryService->setNewOrderRequest($values["barcode"], $values['amount'], $ownerLoginName);

		$backlink = empty($values["backlink"]) ? "Detail:action" : $values["backlink"];
		$blparams = empty($values["blparams"]) ? ["barcode" => $values["barcode"]] : $this->stringToArray($values["blparams"]);
		$this->redirect($backlink, $blparams);
	}

	function stringToArray($strArray) {
		$mstr = explode(",",$strArray);
		$a = array();
		foreach($mstr as $nstr )
		{
				$narr = explode("=>",$nstr);
		$narr[0] = str_replace("\x98","",$narr[0]);
		$ytr[1] = $narr[1];
		$a[$narr[0]] = $ytr[1];
		}
		return $a;
	}

}
