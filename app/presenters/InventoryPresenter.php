<?php

namespace App\Presenters;
use Nette\Application\UI;
use App\Components\InventoryForm;

class InventoryPresenter extends AbstractProductAmountModificationPresenter
{
	protected $pageName = "Inventura";
	protected $pageColor = "blue lighten-1";
	protected $pageIcon = "img/inventory.png";

	function onInventorySubmitted(InventoryForm $form, $values)
	{
		$this->inventoryService->setAmount($values["barcode"], $values['amount']);
		$itemInfo = $this->inventoryService->getInfo($values["barcode"]);

		$amountFloatWithPoint = $this->amountFloatWithPoint($itemInfo["quantity"]);
		if(isset($itemInfo) && $amountFloatWithPoint == $values['amount']) {
			$this->checkStoreLimit($values["barcode"], "Inventory:search");
			$this->flashMessage("Nastaven počet kusů");
			$this->redirect("search", ["barcode"=>$values["barcode"]]);
		}
		else {
			$this->flashMessage("Nastavení početu kusů se nezdařilo. Opakujte zadání znovu.");
			$this->redirect("search", ["barcode"=>$values["barcode"]]);
		}
	}
}
