<?php

namespace App\Presenters;
use Nette\Application\UI;
use App\Components\InventoryForm;

class InventoryPlacePresenter extends AbstractProductAmountPlaceModificationPresenter
{
	protected $pageName = "Inventura + přemístění";
	protected $pageColor = "blue lighten-4";
	protected $pageIcon = "img/inventory-move.png";

	function onPlaceSubmitted(UI\Form $form, $values)
	{
		$placeInfo = $this->inventoryService->getPlaceInfo($values["placecode"]);
		if (empty($placeInfo)) {
			$this->flashMessage("Umístění nenalezeno");
			$this->redirect("action", ["barcode"=>$values["barcode"]]);
		} else {
			$this->redirect("action2", ["barcode"=>$values["barcode"], "newplaceid"=>$placeInfo["id"], "newplacelabel"=>$placeInfo["code"]]);
		}
	}

	function renderAction2($barcode, $newplaceid, $newplacelabel)
	{
		$this->template->itemInfo = $this->inventoryService->getInfo($barcode);
		$this->template->newplacelabel = $newplacelabel;
		$this->template->pageName = $this->pageName;
		$this->template->pageIcon = $this->pageIcon;
		$this->template->pageColor = $this->pageColor;
	}

	function onInventorySubmitted(InventoryForm $form, $values)
	{
		$this->inventoryService->setAmountAndPlace($values["barcode"], $values['placeid'], $values['amount']);
		$itemInfo = $this->inventoryService->getInfo($values["barcode"]);

		$amountFloatWithPoint = $this->amountFloatWithPoint($itemInfo["quantity"]);
		if(isset($itemInfo) && $amountFloatWithPoint == $values['amount']) {
			$this->checkStoreLimit($values["barcode"], "InventoryPlace:search");
			$this->flashMessage("Nastaven počet kusů a umístění");
			$this->redirect("search", ["barcode"=>$values["barcode"]]);
		}
		else {
			$this->flashMessage("Nastavení početu kusů se nezdařilo. Opakujte zadání znovu.");
			$this->redirect("search", ["barcode"=>$values["barcode"]]);
		}
	}

}
