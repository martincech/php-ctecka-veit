<?php

namespace App\Presenters;
use Nette\Application\UI;
use App;

abstract class AbstractProductSearchPresenter extends UI\Presenter
{
	/** @var \App\Models\InventoryService @inject */
	public $inventoryService;

	protected $pageName;
	protected $pageIcon;
	protected $pageColor;
	protected $detailAction;

	function renderSearch()
	{
		if (isset($this->request->parameters["barcode"])) {
			$barcode = $this->request->parameters["barcode"];
			$this->template->itemInfo = $this->inventoryService->getInfo($barcode);
		}
		$this->template->pageName = $this->pageName;
		$this->template->pageIcon = $this->pageIcon;
		$this->template->pageColor = $this->pageColor;
	}

	function createComponentSearchForm()
	{
		$form = new UI\Form;
		$form->addText("barcode")
			->setAttribute("class", "validate")
			->setAttribute("autocomplete", "off")
			->setRequired("Toto pole je povinné");
		$form->addButton("submit", "Hledat")
			->setAttribute("class", "btn waves-effect waves-light text-center")
			->setAttribute("type", "submit");
		$form->onSuccess[] = [$this, "onSearchSubmitted"];
		return $form;
	}

	function onSearchSubmitted(UI\Form $form, $values)
	{
		$info = $this->inventoryService->getInfo($values["barcode"]);
		if (empty($info)) {
			$this->flashMessage("Výrobek nenalezen");
			$this->redirect("search");
		} else {
			$this->redirect("action", ["barcode"=>$values["barcode"]]);
		}
	}

	function renderAction($barcode)
	{
		$this->template->itemInfo = $this->inventoryService->getInfo($barcode);
		$this->template->pageName = $this->pageName;
		$this->template->pageIcon = $this->pageIcon;
		$this->template->pageColor = $this->pageColor;
	}

	function amountFloatWithPoint($amount){
		// convert ABRA format with decimal comma to decimal point
		return preg_replace("/[^-0-9\.]/",".",$amount);
	}

	function redirectToSearch($flashMessage)
	{
		$this->flashMessage($flashMessage);
		$this->redirect("search");
	}

	function checkStoreLimit($barcode, $backlink = '', $backlinkParams = "")
	{
		$itemInfo = $this->inventoryService->getInfo($barcode);
		
		$quantity = (float)$this->amountFloatWithPoint($itemInfo['quantity']);		
		$ordered = (float)$this->amountFloatWithPoint($itemInfo["quantity_ordered"]);
		$demanded = (float)$this->amountFloatWithPoint($itemInfo["quantity_demanded"]);

		$onTheWay = $ordered + $demanded;
		$totalQuantity = $quantity + $ordered + $demanded; 
		$lowLimit = (float)$this->amountFloatWithPoint($itemInfo['low_limit']);

		if ( ($totalQuantity < $lowLimit) )
		{
			$this->flashMessage("Stav zásob položky se dostal pod spodní limit.");
			$this->redirect("OrderRequest:action", ["barcode"=>$barcode, "backlink"=>$backlink, "blparams"=>$backlinkParams]);
		}
	}

}
