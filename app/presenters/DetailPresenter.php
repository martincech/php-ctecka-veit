<?php

namespace App\Presenters;

class DetailPresenter extends AbstractProductSearchPresenter
{
	protected $pageName = "Detail";
	protected $pageColor = "yellow darken-4";
	protected $pageIcon = "img/detail.png";
}

