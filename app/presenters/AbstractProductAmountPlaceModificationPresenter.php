<?php

namespace App\Presenters;
use App\Components\InventoryForm;
use App\Components\PlaceForm;

abstract class AbstractProductAmountPlaceModificationPresenter extends AbstractProductSearchPresenter
{
	protected $barcode;

	function createComponentPlaceForm()
	{
		if (!isset($this->barcode) && isset($this->request->parameters["barcode"])) {
			$this->barcode = $this->request->parameters["barcode"];
		}
		return new PlaceForm($this, $this->barcode);
	}

	function createComponentInventoryForm()
	{
		if (!isset($this->barcode) && isset($this->request->parameters["barcode"])) {
			$this->barcode = $this->request->parameters["barcode"];
		}
    $placeId = isset($this->request->parameters["newplaceid"]) ? $this->request->parameters["newplaceid"] : null;
		
    return new InventoryForm($this, $this->barcode, $placeId);
	}

	abstract function onInventorySubmitted(InventoryForm $form, $values);
}
