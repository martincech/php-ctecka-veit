<?php

namespace App\Components;
use Nette\Application\UI;

class OrderPrefixForm extends UI\Form
{
	function __construct(UI\presenter $presenter, $ordercode, $orderyear = '') {
		$form = new UI\Form;

		$this->addText("orderprefix")
			->setAttribute("class", "validate")
			->setAttribute("autocomplete", "off")
			->setRequired("Vyberte řadu volbou čísla 1 nebo 2")
			->addRule(UI\Form::PATTERN, 'Vyberte řadu volbou čísla 1 nebo 2', '([1-2]\s*){1}');
		$this->addHidden("ordercode");
		$this->addHidden("orderyear");
		$this->addButton("submit", "Zvolit řadu")
			->setAttribute("class", "btn waves-effect waves-light")
			->setAttribute("type", "submit");

		$this->setDefaults(["ordercode"=>$ordercode]);
		$this->setDefaults(["orderyear"=>$orderyear]);
		$this->onSuccess[] = [$presenter, "onOrderPrefixSubmitted"];

		return $form;
	}
}
