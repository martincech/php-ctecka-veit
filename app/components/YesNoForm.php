<?php

namespace App\Components;
use Nette\Application\UI;

class YesNoForm extends UI\Form
{
	function __construct(UI\presenter $presenter, $onSubmitMethodName = "onYesNoSubmitted") {
		$form = new UI\Form;

		$this->addText("answer")
			->setAttribute("class", "validate")
			->setAttribute("autocomplete", "off")
			->setRequired("Zadejte 1 pro volbu ANO, nebo 0 pro volbu NE")
			->addRule(UI\Form::PATTERN, 'Zadejte 1 pro volbu ANO, nebo 0 pro volbu NE', '([0-1]\s*){1}');
		$this->addButton("submit", "Potvrdit")
			->setAttribute("class", "btn waves-effect waves-light")
			->setAttribute("type", "submit");

		// $this->setDefaults(["answer"=>1]);
		$this->onSuccess[] = [$presenter, $onSubmitMethodName];

		return $form;
	}
}
