<?php

namespace App\Components;
use Nette\Application\UI;

class SearchOrderForm extends UI\Form
{
	function __construct(UI\presenter $presenter) {
		$form = new UI\Form;
		$this->addText("ordercode")
			->setAttribute("class", "validate")
			->setAttribute("autocomplete", "off")
			->setRequired("Toto pole je povinné")
			->addRule(UI\Form::PATTERN, 'Zadejte číslo nebo celý kód objednávky (123 nebo OV1-123/2015).', '([OVov]{2}[1-2]-\d+/\d{4})|^\d+$');
		$this->addButton("submit", "Hledat objednávku")
			->setAttribute("class", "btn waves-effect waves-light")
			->setAttribute("type", "submit");
		$this->onSuccess[] = [$presenter, "onSearchOrderSubmitted"];
		return $form;
	}
}
