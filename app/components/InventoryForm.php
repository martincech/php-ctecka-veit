<?php

namespace App\Components;
use Nette\Application\UI;

class InventoryForm extends UI\Form
{
	// $this->request->getParameters()["barcode"]]
	// $this
	function __construct(UI\presenter $presenter, $barcode, $placeid = null, $orderitemid = null) {
		$backlink = isset($presenter->request->parameters["backlink"]) ? $presenter->request->parameters["backlink"] : null;
		$blparams = isset($presenter->request->parameters["blparams"]) ? $presenter->request->parameters["blparams"] : null;

		$form = new UI\Form;
		$this->addText("amount")
			->setAttribute("class", "validate")
			->setAttribute("autocomplete", "off")
			->setRequired("Toto pole je povinné")
			->addRule(UI\Form::FLOAT, "Musí být číslo")
			->addRule(UI\Form::RANGE, 'Musí být kladné číslo', array(0, null));
		$this->addHidden("barcode")
			->setRequired();
		$this->addHidden("placeid");
		$this->addHidden("orderitemid");
		$this->addHidden("backlink");
		$this->addHidden("blparams");
		$this->addButton("submit", "Odeslat")
			->setAttribute("type", "submit");
		$this->setDefaults(["barcode"=>$barcode]);
		$this->setDefaults(["placeid"=>$placeid]);
		$this->setDefaults(["orderitemid"=>$orderitemid]);
		$this->setDefaults(["backlink"=>$backlink]);
		$this->setDefaults(["blparams"=>$blparams]);
		$this->onSuccess[] = [$presenter, "onInventorySubmitted"];
		return $form;
	}
}
