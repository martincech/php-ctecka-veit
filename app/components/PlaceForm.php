<?php

namespace App\Components;
use Nette\Application\UI;

class PlaceForm extends UI\Form
{
	function __construct(UI\presenter $presenter, $barcode = null) {
		$form = new UI\Form;
		$this->addText("placecode")
			->setAttribute("class", "validate")
			->setAttribute("autocomplete", "off")
			->setRequired("Toto pole je povinné");
		$this->addHidden("barcode");
		$this->addButton("submit", "Hledat umístění")
			->setAttribute("class", "btn waves-effect waves-light")
			->setAttribute("type", "submit");

		$this->setDefaults(["barcode"=>$barcode]);
		$this->onSuccess[] = [$presenter, "onPlaceSubmitted"];
		return $form;
	}
}
