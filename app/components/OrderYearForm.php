<?php

namespace App\Components;
use Nette\Application\UI;

class OrderYearForm extends UI\Form
{
	function __construct(UI\presenter $presenter, $ordercode) {
		$form = new UI\Form;

		$this->addText("orderyear")
			->setAttribute("class", "validate")
			->setAttribute("autocomplete", "off")
			->setRequired("Zadejte rok, např.: 2016")
			->addRule(UI\Form::PATTERN, 'Zadejte rok, např.: 2016', '20\d{2}');
		$this->addHidden("ordercode");
		$this->addButton("submit", "Zvolit rok")
			->setAttribute("class", "btn waves-effect waves-light")
			->setAttribute("type", "submit");

		$this->setDefaults(["ordercode"=>$ordercode]);
		$this->onSuccess[] = [$presenter, "onOrderYearSubmitted"];

		return $form;
	}
}
